<?php

namespace Database\Seeders;

use App\Models\Tweet;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        /** @var User $user */
        $user = User::factory()
            ->has(Tweet::factory()->count(100))
            ->create([
                         'name'              => "root",
                         'email'             => "root@mail.com",
                         'email_verified_at' => now(),
                         'password'          => Hash::make('password'),
                         'remember_token'    => Str::random(10),
                     ]);
        User::factory()
            ->has(Tweet::factory()->count(100))
            ->count(100)->create();

        $user->followings()->attach(User::all());
    }
}
