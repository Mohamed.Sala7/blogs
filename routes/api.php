<?php

use App\Http\Controllers\Api\FollowerController;
use App\Http\Controllers\Api\TweetController;
use App\Http\Controllers\Api\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:sanctum');

Route::post('/auth/register', [ UserController::class, 'createUser']);
Route::post('/auth/login', [UserController::class, 'loginUser']);

Route::group(['middleware' => 'auth:sanctum'],static function () {
    Route::get("/my-tweets",[TweetController::class,"index"]);
    Route::get("/tweets/timeline",[TweetController::class,"timeline"]);
    Route::get("/tweets/user/{user_id}",[TweetController::class,"userTweets"]);
    Route::resource('/tweet', TweetController::class)
         ->only('store', 'update','destroy');

    Route::post('/follow/{user}',[FollowerController::class,'store']);
    Route::delete('/unfollow/{user}',[FollowerController::class,'destroy']);

});
