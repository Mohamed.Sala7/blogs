<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Follower;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class FollowerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param User $user
     * @return JsonResponse
     */
    public function store(User $user): JsonResponse
    {
        /** @var User $follower */
        $follower = Auth::user();
        $follower->followings()->attach($user);
        return response()->json([
                'status'  => true,
                'message' => 'operation done successfully',
            ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(Follower $follower)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @return JsonResponse
     */
    public function destroy(User $user): JsonResponse
    {
        /** @var User $follower */
        $follower = Auth::user();
        $follower->followings()->detach($user);
        return response()->json([
                'status'  => true,
                'message' => 'operation done successfully',
            ]);
    }
}
