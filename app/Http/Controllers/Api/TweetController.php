<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreTweetRequest;
use App\Http\Requests\UpdateTweetRequest;
use App\Http\Resources\TweetResource;
use App\Models\Tweet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
class TweetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param $query
     * @return AnonymousResourceCollection
     */
    public function index(Request $request): AnonymousResourceCollection
    {
        $tweets = Tweet::search($request->input('q'))->where('user_id', Auth::id())
                       ->orderBy('id:desc')->paginate();
        return TweetResource::collection($tweets);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreTweetRequest $request
     * @return TweetResource
     */
    public function store(StoreTweetRequest $request): TweetResource
    {
        /** @var User $user */
        $user = Auth::user();
        $tweet = $user->tweets()->create($request->validated());
        return new TweetResource($tweet);
    }

    /**
     * Display the specified resource.
     */
    public function show(Tweet $tweet)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateTweetRequest $request
     * @param Tweet $tweet
     * @return TweetResource
     */
    public function update(UpdateTweetRequest $request, Tweet $tweet): TweetResource
    {
        $tweet->update($request->validated());
        return new TweetResource($tweet);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Tweet $tweet
     * @return JsonResponse
     */
    public function destroy(Tweet $tweet): JsonResponse
    {
        Gate::authorize('delete', $tweet);
        $tweet->delete();
        return response()->json([
                'status'  => true,
                'message' => 'Tweet deleted successfully',
            ]);
    }

    /**
     * @param Request $request
     * @return AnonymousResourceCollection
     */
    public function timeline(Request $request): AnonymousResourceCollection
    {
        /** @var User $user */
        $user = Auth::user();
        $followings = $user->followings()->get(['users.id'])->makeHidden('pivot')
                    ->pluck('id')->toArray();
        $tweets = Tweet::search($request->input('q'))->whereIn('user_id', $followings)->paginate();
        return TweetResource::collection($tweets);
    }

    /**
     * @param $user_id
     * @param Request $request
     * @return AnonymousResourceCollection
     */
    public function userTweets($user_id, Request $request): AnonymousResourceCollection
    {
        $tweets = Tweet::search($request->input('q'))->where('user_id', $user_id)->paginate();
        return TweetResource::collection($tweets);
    }
}
