<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Mockery\Exception;
use Illuminate\Http\JsonResponse;
class UserController extends Controller
{
    /**
     * Create User
     * @param RegisterRequest $request
     * @return JsonResponse
     */
    public function createUser(RegisterRequest $request): JsonResponse
    {
        $saved_file=null;
        if($request->has('profile_img')){
            try{
                $image      = $request->file('profile_img');
                $name       = time().".".$image->getClientOriginalExtension();
                $saved_file = Storage::disk('public')->putFileAs('/profile_img', $image, $name, 'public');
                if(!$saved_file){
                    return response()->json([
                            'status' => false,
                            'message' => "try again later",
                        ], Response::HTTP_BAD_REQUEST);
                }
            }catch(Exception $exception){
                return response()->json([
                            'status' => false,
                            'message' => $exception->getMessage(),
                        ], $exception->getCode());
            }
        }

        $user = User::create([
                     'name'     => $request->input('username'),
                     'email'    => $request->input('email'),
                     'password' => Hash::make($request->input('password')),
                     'img_url' => $saved_file
                 ]);
        return response()->json([
                    'status'  => true,
                    'message' => 'User Created Successfully',
                    'token'   => $user->createToken("API TOKEN")->plainTextToken,
                    'user'    => new UserResource($user),
                ], Response::HTTP_CREATED);
    }

    /**
     * Login The User
     * @param LoginRequest $request
     * @return JsonResponse
     */
    public function loginUser(LoginRequest $request)
    {
            if(!Auth::attempt($request->only([ 'email', 'password' ]))){
                return response()->json([
                    'status' => false,
                    'message' => 'Email or Password does not match with our record.',
                ], Response::HTTP_BAD_REQUEST);
            }

            $user = User::where('email', $request->email)->first();

            return response()->json([
                'status'  => true,
                'message' => 'User Logged In Successfully',
                'token'   => $user->createToken("API TOKEN")->plainTextToken,
                'user'    => new UserResource($user),
            ]);

    }
}