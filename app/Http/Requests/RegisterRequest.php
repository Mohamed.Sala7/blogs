<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rules\Password;
use Illuminate\Contracts\Validation\ValidationRule;

class RegisterRequest extends CustomFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'email'       => 'required|unique:users|email',
            'username'    => 'required|alpha_dash|max:255',
            'password'    => [ 'required', 'confirmed', Password::min(8)->mixedCase()->numbers()->symbols() ],
            'profile_img' => 'image|max:1024|mimes:png,jpg'
        ];
    }
}
