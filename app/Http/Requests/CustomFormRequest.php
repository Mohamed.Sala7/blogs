<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;

class CustomFormRequest extends FormRequest
{
    /**
     * Validation Failed.
     *
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator) : void
    {
        throw new HttpResponseException(
            response()->json([
                                 'status' => false,
                                 'errors' => $validator->getMessageBag()
                             ], Response::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}
