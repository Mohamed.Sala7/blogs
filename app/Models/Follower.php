<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int id
 * @property int follower_id
 * @property int destination_id
 */

class Follower extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function follower(): BelongsTo
    {
        return $this->belongsTo(User::class,'follower_id');
    }

    public function destinationUser(): BelongsTo
    {
        return $this->belongsTo(User::class,'destination_id');
    }

}
