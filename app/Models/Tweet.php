<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Scout\Searchable;

/**
 * @property int id
 * @property int user_id
 * @property string content
 */

class Tweet extends Model
{
    use HasFactory, SoftDeletes, Searchable;
    protected $guarded = ['id'];

    protected $with = [ 'owner' ];

    public function owner(): BelongsTo
    {
        return $this->belongsTo(User::class,'user_id');
    }

}
