<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Storage;
use Laravel\Sanctum\HasApiTokens;

/**
 * @property int id
 * @property string name
 * @property string email
 * @property string img_url
 *
 * @property Tweet|Tweet[] tweets
 * @property User|User[] followings
 * @property User|User[] followers
 */
class User extends Authenticatable
{
    use HasFactory, Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [ 'name', 'email', 'password', 'img_url', ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [ 'password', 'remember_token', 'pivot' ];

    /**
     * Get the attributes that should be cast.
     *
     * @return array<string, string>
     */
    protected function casts(): array
    {
        return [
            'email_verified_at' => 'datetime',
            'password'          => 'hashed',
        ];
    }

    public function profileImage(): ?string
    {
        return $this->img_url ? Storage::disk('public')->url($this->img_url):null;
    }

    public function tweets(): HasMany
    {
        return $this->hasMany(Tweet::class);
    }

    public function followings(): BelongsToMany
    {
        return $this->belongsToMany(User::class,'followers','follower_id','destination_id');
    }

    public function followers(): BelongsToMany
    {
        return $this->belongsToMany(User::class,'followers','destination_id','follower_id');
    }
}
