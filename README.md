## Install project
## Run Docker Containers
### Requirements
1. Docker.
2. Docker compose.

### Installation Steps:
1. The project root is where "docker-compose.yml" is placed.
2. docker compose build
3. docker compose up -d

## Run Laravel
### Installation Steps:
1. docker compose exec blogs-app bash
2. cp .env.example .env
3. composer i
4. php artisan key:generate


## Run Project
1. php artisan migrate 
2. php artisan db:seed
3. php artisan scout:sync-index-settings
4. php artisan scout:import App\\\Models\\\Tweet
5. php artisan queue:work redis --queue=scout

## Usage
* run postman collection attached to project.



